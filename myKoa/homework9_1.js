const fs = require("fs");

function readHead() {
  return new Promise(function(resolve, reject) {
    fs.readFile("head.txt", "utf8", function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

function readBody() {
  return new Promise(function(resolve, reject) {
    fs.readFile("body.txt", "utf8", function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

function readLeg() {
  return new Promise(function(resolve, reject) {
    fs.readFile("leg.txt", "utf8", function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

function readFeet() {
  return new Promise(function(resolve, reject) {
    fs.readFile("feet.txt", "utf8", function(err, data) {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

function writeRobot(robot) {
    return new Promise(function(resolve, reject) {
      fs.writeFile('robot2.txt', robot, 'utf8', function(err) {
        if (err)
          reject(err);
        else
          resolve();
      });
    });
}

let robot = "";
async function myRobot() {
  try {
    let head = await readHead();
    robot += head + '\n';
    let body = await readBody();
    robot += body + '\n';
    let leg = await readLeg();
    robot += leg + '\n';
    let feet = await readFeet();
    robot += feet + '\n';
    console.log(robot);
    await writeRobot(robot);
  } catch (error) {
    console.error(error);
  }
}
myRobot();
