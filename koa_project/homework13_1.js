'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool  = mysql.createPool({
   connectionLimit : 10,
   host            : 'localhost',
   user            : 'root',
   password        : 'nI<5!!My$q1',
   database        : 'codecamp'
});

router.get('/',async ctx =>{
    const results = await getCourses()
    let objData = {}
    objData.course = results

    const results2 = await getInstructors();
    let obj = {}
    obj.instructor = results2
    console.log({course:objData.course,instructor:obj.instructor})
    await ctx.render('homework13_1',{course:objData.course,instructor:obj.instructor})
})

async function getCourses() {
    let [rows, fields] = await pool.query(`SELECT i.name AS name_instructors,c.name AS name_courses
                                            FROM instructors AS i
                                            LEFT JOIN courses AS c 
                                            ON c.teach_by=i.id WHERE c.name 
                                            IS NULL`);
    console.log("COURSE : ",rows)
    return rows;
}

async function getInstructors() {
    let [rows, fields] = await pool.query(`SELECT courses.name AS course_name, instructors.name AS instructor_name
                                            FROM courses LEFT JOIN instructors 
                                            ON instructors.id = courses.teach_by
                                            WHERE instructors.name IS NULL`);
    console.log("INSTRUCTORS : ",rows)
    return rows;
}

app.use(serve(path.join(__dirname,"public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(4000)

