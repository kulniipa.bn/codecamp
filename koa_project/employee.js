class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        console.log("SALARY : ",this._salary)
        this.firstname = firstname
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        //getSalary()
        console.log("set salary new salary $$$$ ",newSalary)
        console.log("this salary $$$$ " , this._salary)
        if(newSalary > this._salary){
            //console.log(this.firstname + "i's salary has been set to " + newSalary)
            return newSalary
        }else if(newSalary <= this._salary){
            //console.log(this.firstname + "'s salary is less than before!!")
            return false;
        }
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
    }
    getSalary() {  // simulate public method
        console.log('BBDSDFSDFE ',this._salary)
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {
    }
}
exports.Employee = Employee;