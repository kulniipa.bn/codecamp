'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const fs = require('fs')

const app = new Koa()
const router = new Router()

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'nI<5!!My$q1',
    database: 'codecamp'
});

async function getUser() {
    let [rows, fields] = await pool.query('SELECT * FROM user');
    return rows;
}

router.get('/from_database', async (ctx, next) => {
    const results = await getUser();
    ctx.body = results;
    ctx.myVariable = results;
    await next();
})

function getData() {
    return new Promise(function (resolve, reject) {
        fs.readFile('homework2_1.json', 'utf8', function (err, DataLeg) {
            if (err) {
                reject(err);
                return
            }
            resolve(DataLeg);
        });
    });
}

async function readFiles() {
    try {
        const strData = await getData();
        return strData

    } catch (error) {
        console.error(error);
    }
}

router.get('/from_file', async (ctx, next) => {
    const result = await readFiles()
    ctx.body = JSON.parse(result)
    ctx.myVariable = JSON.parse(result)
    await next()
})

async function additional(ctx, next) {
    let objData = ctx.myVariable
    let obj = {}
    let objAdditional = {}
    objAdditional.userId = 1
    let dt = new Date();
    objAdditional.dateTime = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate() + 
                             " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds(); 
    obj.data = ctx.myVariable
    obj.additionalData = objAdditional
    console.log(obj)
    ctx.body = obj
    await next()
}

app.use(router.routes())
app.use(additional)

app.use(serve(path.join(__dirname, "public")))
app.use(router.allowedMethods())
app.listen(4000)