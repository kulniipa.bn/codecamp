'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'nI<5!!My$q1',
    database: 'codecamp'
});

router.get('/books', async ctx => {
    const results = await getBooks();
    let objData = {};
    objData.user = results
    console.log(results);
    await ctx.render('homework12_1', objData)
})

router.get('/employees', async ctx => {
    const results = await getEmployees();
    let objData = {}
    objData.user = results
    console.log(results)
    await ctx.render('homework12_1', objData)
})

async function getEmployees() {
    let [rows, fields] = await pool.query('SELECT firstname,lastname,age FROM employees');
    console.log('Employee : ', rows, fields)
    return rows
}

async function getBooks() {
    let [rows, fields] = await pool.query('SELECT isbn,title,price FROM books');
    console.log('Books : ', rows, fields)
    return rows
}

app.use(serve(path.join(__dirname, "public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(5000)