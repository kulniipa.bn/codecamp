'use strict'

const pool = require('../lib/db')
async function getUser() {
    let [rows, fields] = await pool.query('SELECT * FROM user');
    return rows;
}

module.exports = getUser