const bcrypt = require('bcrypt')

async function hashPassword(){
    const password = 'superman'
    const hashedPassword = await bcrypt.hash(password, 10)
    console.log(hashedPassword)
}

async function comparePassword(){
    const hashed_password = '$2a$10$Q49GCf4uEOVoBCVqlaPmeOs481Jz2ygQN4GaIaDhLqjFC2gtY7sZq'
    const same = await bcrypt.compare('superman', hashed_password)
    if (!same) {
    console.log('wrong password')
    } else {
    console.log('password correct')
    }
}
hashPassword()
comparePassword()

