const Koa = require('koa')
const session = require('koa-session')

const app = new Koa()

const sessionConfig = {
    key: 'sess',
    maxAge: 1000 * 60 * 60,
    httpOnly: true
}

app.keys = ['supersecret']
app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)

function handler(ctx) {
    // ignore favicon
    if (ctx.path === '/favicon.ico') return
    let n = ctx.session.views || 0
    ctx.session.views = ++n
    ctx.body = `${n} views`
}