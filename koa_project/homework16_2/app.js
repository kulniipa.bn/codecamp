const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path =  require('path')
const serve = require('koa-static')

const app = new Koa()
const router = new Router()

render(app,{
    root: path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool = require('../lib/db.js')
const instructorModel = require('./models/instructor.js')
const courseModel = require('./models/course.js')
router.get('/instructor/find_all',async (ctx,next)=>{
    ctx.body = await instructorModel.getInstructorAll(pool)
    await next()
})

router.get('/instructor/find_by_id/:id',async (ctx,next)=>{
    ctx.body = await instructorModel.getInstructorById(pool,ctx.params.id)
    await next()
})

router.get('/course/find_by_id/:id',async (ctx,next)=>{
    ctx.body = await courseModel.getCourseById(pool,ctx.params.id)
    await next()
})

router.get('/course/find_by_price/:price',async (ctx,next)=>{
    ctx.body = await courseModel.getCourseByPrice(pool,ctx.params.price)
    console.log(await courseModel.getCourseByPrice(pool,ctx.params.price))
    //let course_price = await courseModel.getCourseByPrice(pool,ctx.params.price)
    //let objCoursePrice = {}
    //objCoursePrice.objData = course_price
    //console.log(objCoursePrice)
    //await ctx.render('table',objCoursePrice)
    await next()
})

app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(4000)


