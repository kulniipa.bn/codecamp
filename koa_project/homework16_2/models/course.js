module.exports = {
    async createEntity(row){
        return{
            id:row.id,
            name:row.name,
            created_at:row.created_at
        }
    },
    async getCourseById(pool,id){
        const [rows] = await pool.query("SELECT * FROM courses WHERE id=?",[id])
        return this.createEntity(rows[0])
    },
    async getCourseByPrice(pool,price){
        const [rows] = await pool.query("SELECT * FROM courses WHERE price=?",[price])
        //console.log(rows)
        let courseObj = {}
        courseObj.jsonData = rows
        console.log(courseObj)
        return this.createEntity(courseObj)
    }
}