const Koa = require("koa");
const Router = require("koa-router");
const render = require("koa-ejs");
const serve = require("koa-static");
const path = require("path");
const mysql = require("mysql2/promise");
const koaBody = require("koa-body");
const bcrypt = require("bcrypt");
const session = require("koa-session");
const app = new Koa();
const router = new Router();

app.keys = ["supersecret"];
const sessionStore = {
  key: "sess",
  maxAge: 1000 * 60 * 60,
  httpOnly: true,
  store: {
    get(key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set(key, sess, maxAge, { rolling }) {
      sessionStore[key] = sess;
    },
    destroy(key) {
      delete sessionStore[key];
    }
  }
};

render(app, {
  root: path.join(__dirname, "view"),
  layout: "template",
  viewExt: "ejs",
  cache: false
});

const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "nI<5!!My$q1",
  database: "db2"
});

router.get("/employee", async (ctx, next) => {
  const [employees] = await pool.query(`select e.id As 'Employee ID',
                                                e.first_name AS 'Firstname',
                                                e.last_name As 'Lastname',
                                                j.job_name As 'Job Name',
                                                '' As Photo 
                                            from 
                                                employee as e
                                            inner join 
                                                job as j
                                            on e.job_id=j.id`);

  const [jobs] = await pool.query(`select j.id as 'Job ID',j.job_name as 'Job Name' 
                                        from 
                                            employee as e
                                        right join 
                                            job as j
                                        on 
                                            e.job_id=j.id
                                        where 
                                            e.id is null`);
  await ctx.render("employees", {
    employees: employees,
    jobs: jobs
  });
});

//register jquery
router.get("/register_jquery", async (ctx, next) => {
  await ctx.render("register_jquery");
});

//register koa-ejs
router.get("/register", async (ctx, next) => {
  await ctx.render("register");
});

router.post("/register_completed", async (ctx, netx) => {
  let username = ctx.request.body.username;
  let password = ctx.request.body.password;
  let confirmPassword = ctx.request.body.confirm_password;
  let phone = ctx.request.body.phone;
  let confirmPhone = ctx.request.body.confirm_phone;
  const hashedPassword = await bcrypt.hash(password, 10);
  const userData = [username, hashedPassword, phone]
  const [rows] = await pool.query(`INSERT INTO user(username, password, phone) VALUES (?,?,?)`,userData);

  await ctx.render("register_completed", {
    id: rows.insertId,
    username: username
  });
});

router.get("/login", async (ctx, next) => {
  await ctx.render("login", {
    message: "",
    link: ""
  });
});

router.post("/login", async (ctx, next) => {
  let username = ctx.request.body.username;
  let password = ctx.request.body.password;
  const [rows] = await pool.query(`SELECT id,username,password FROM user where username=?`,[username]);

  if (rows.length > 0) {
    const decrypPassword = await bcrypt.compare(password, rows[0].password);
    if (!decrypPassword || username !== rows[0].username) {
      await ctx.render("login", {
        message: "Username or Password Wrong",
        link: ''
      });
      //ctx.redirect('login_completed');
    } else {
      ctx.session.userId = rows[0].id;
      ctx.body = ctx.session.userId;
      ctx.redirect("/profile");
    }
  } else {
    await ctx.render("login", {
        message: "Couldn't find your account.",
        link:  "<a href='/register'>register</a>"
    });
    //ctx.redirect("/register");
  }
});

const checkAuth = async (ctx, next) => {
  if (!ctx.session || !ctx.session.userId) {
    return ctx.redirect("login");
  } else {
    await next();
  }
};

router.get("/profile", checkAuth, async (ctx, next) => {
  const userId = ctx.session.userId;
  const [rows] = await pool.query(`SELECT id,username,phone FROM user where id=?`,[userId]);

  await ctx.render("profile", {
    id: rows[0].id,
    username: rows[0].username,
    phone: rows[0].phone
  });
});

router.get('/login_completed',async(ctx,next)=>{
    await ctx.render("login_completed");
})
router.get("/logout", async (ctx, next) => {
  ctx.session = null;
  await ctx.render("logout");
});

router.get("/data", async (ctx, next) => {
  await ctx.render("await");
});

app.use(session(sessionStore, app));
app.use(koaBody());
app.use(serve(path.join(__dirname, "public")));
app.use(router.routes());
app.listen(4000);
