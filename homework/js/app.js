debugger;
//Chaining
let animals = ["cat","dog","fish"];
function studlyCaps(words, word) {
  return words + word;
}
function exactlyThree(word) {
  return (word.length === 3);
}
function capitalize(word) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}
const threeLetterAnimals = animals
  .filter(exactlyThree)
  .map(capitalize)
  .reduce(studlyCaps);
console.log(threeLetterAnimals); // "CatDog"

//filter function
let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
const result = words.filter(word => {
  return word.length > 6
});
console.log(result); // ["exuberant", "destruction", "present"]

//map function
let people=['a','b'];
let people2 = people.map(function(item){
    return `Mr. ${item}`;
})
console.log(people2);

