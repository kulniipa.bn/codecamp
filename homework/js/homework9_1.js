'use strict'

let fs = require('fs');

function readHead() {
  return new Promise(function(resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, DataHead) {
      if (err)
        reject(err);
      else
        resolve(DataHead);
    });
  });
}

function readBody() {
  return new Promise(function(resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err, DataBody) {
      if (err)
        reject(err);
      else
        resolve(DataBody);
    });
  });
}

function readLeg() {
  return new Promise(function(resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, DataLeg) {
      if (err)
        reject(err);
      else
        resolve(DataLeg);
    });
  });
}

function readFeet() {
  return new Promise(function(resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, DataFeet) {
      if (err)
        reject(err);
      else
        resolve(DataFeet);
    });
  });
}

function writeRobot(StrRobot) {
    return new Promise(function(resolve, reject) {
      fs.writeFile('robot.txt', StrRobot, 'utf8', function(err) {
        if (err)
          reject(err);
        else
          resolve();
      });
    });
}
  
async function ReadWriteFiles() {
    try {
      const StrHead = await readHead();
      const StrBody = await readBody();
      const StrLeg = await readLeg();
      const StrFeet = await readFeet();
      const StrRobot = `${StrHead}\n${StrBody}\n${StrLeg}\n${StrFeet}\n` 
      await writeRobot(StrRobot);
    } catch (error) {
      console.error(error);
    }
}
ReadWriteFiles();
  