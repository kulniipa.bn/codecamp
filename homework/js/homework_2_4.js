getTable();
function getTable(){
    $(document).ready(function() {

      $.getJSON("data/homework1-4.json", function (data) {
        console.log(data);
        let population=data;
        
        //create header table
        let personInfo=['Photo','Name','Friend Count','Email','Company','Balance'];
        $('#myTable').append($('<tr>'));
        for (let k in personInfo) {
            console.log(personInfo[k]);
            $('#myTable').append($('<th>' + personInfo[k] + '</th>'));
        }
        $('#myTable').append($('</tr>'));

        //population
        let CountEyeColor={};
        let brown=0;
        let green=0;
        let blue=0;

        let CountGender={};
        let male=0;
        let female=0;
        for (let person in population) {
            //console.log(population[person]['eyeColor']);

            //count eye color
            let eyeColor=population[person]['eyeColor'];
            if(eyeColor==="brown"){
              brown=brown+1
            }else if(eyeColor==="green"){
              green=green+1
            }else if(eyeColor==="blue"){
              blue=blue+1
            }

            //count gender
            let Gender=population[person]['gender'];
            if(Gender==="male"){
              male=male+1
            }else if(Gender==="female"){
              female=female+1
            }

            //personal information
            let history;
            let historyValue=['picture','name','friends','email','company','balance'];
            for (let data in population[person]) {
                console.log(population[person]['eyeColor']);
                let value;
                let CountFreinds=0;
                $('#myTable').append($('<tr>'));
                for (let key in historyValue){
                  console.log(key);
                  value=historyValue[key];
                  if(value==='friends'){
                    for(let f in population[person][value]){
                      CountFreinds=CountFreinds+1;
                    }
                    history=CountFreinds;
                  }else if(value==='picture'){
                    history='<img src="' + population[person][value] + '" height="32" width="32">';
                  }else{
                    history=population[person][value];
                  }          
                  $('#myTable').append($('<td>' + history + '</td>'));
                }
                $('#myTable').append($('</tr>'));
            }//personInfo
        }//population
        //object eyecolor
        CountEyeColor['brown']=brown;
        CountEyeColor['green']=green;
        CountEyeColor['blue']=blue;
        console.log(JSON.stringify(CountEyeColor));

        //object gender
        CountGender.male=male;
        CountGender.female=female;
        console.log(JSON.stringify(CountGender));

        //create eyecolor table
        for (let m in CountEyeColor) {
          console.log(CountEyeColor[m]);
          $('#EyeColor').append($('<tr><td>' + m + '</td>' + '<td>' + CountEyeColor[m] + '</td></tr>'));
        }    

        //create gender table
        for (let n in CountGender) {
          console.log(CountGender[n]);
          $('#Gender').append($('<tr><td>' + n + '</td>' + '<td>' + CountGender[n] + '</td></tr>'));
        }  

      });//getJSON

    });
}

