Employees();
function Employees(){
    $(document).ready(function() {
        let employees;
        fetch('data/homework2_1.json').then(response => {
            return response.json();
        })
        .then(myJson => {
            employees=myJson;
            AddYearSalary(employees[0]);
            AddNextSalary(employees[0]);
            addAdditionalFields(employees);

            let newEmployees = addAdditionalFields(employees);
            
        })
        .catch(error => {
            console.error('Error:', error);
        });


        let YearSalary;
        function AddYearSalary(objEmployee){
            YearSalary=objEmployee['salary'];
            objEmployee.yearSalary=YearSalary*12;   
        }

        function AddNextSalary(objEmployee){
            let year_salary=[];
            let y1,y2,y3;
            let NextSalary=[];
            
            NextSalary=objEmployee['salary'];
            objEmployee.nextSalary=NextSalary;

            y2=objEmployee['salary'];
            console.log(objEmployee['salary']);
            for (let a=0;a<=2;a++){
                if(a==0){
                    year_salary[a]=y2;
                }else if(a>0){
                    y2+=y2*0.1;
                    year_salary[a]=y2;
                }
            }
            objEmployee.nextSalary=year_salary;
        }

        function addAdditionalFields(ObjEmployees){
            let employee;
            let header;
            for(let i in ObjEmployees){
                employee=ObjEmployees[i];
                AddYearSalary(employee);
                AddNextSalary(employee);
            }

            //create table
            $('#myTable').append($('<tr>'));
            for (let k in ObjEmployees[0]) {
                //console.log(k);
                $('#myTable').append($('<th>' + k + '</th>'));    
            }
            $('#myTable').append($('</tr>'));

            let list;
            for (let i in ObjEmployees) {
                //console.log(ObjEmployees[i]);
                $('#myTable').append($('<tr>'));
                for (let j in ObjEmployees[i]) {
                
                        if(j==="nextSalary"){
                            list='<ol>';
                            for(let x=0;x<ObjEmployees[i][j].length;x++){
                                //console.log(ObjEmployees[i][j]);
                                let salary=ObjEmployees[i][j];
                                list+='<li>' + salary[x] + '</li>';
                            }
                            list+='</ol>';
                        }else if(j!=="nextSalary"){
                            list=ObjEmployees[i][j];
                        }
                        $('#myTable').append($('<td>' + list + '</td>'));           
                }
                $('#myTable').append($('</tr>'));
            }//create table 
        }
    });
}