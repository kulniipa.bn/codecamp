$(function () {
    let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // const result_filter = arr.filter(number => {
    //     return number%2===0;
    // });
    // console.log(result_filter);

    // const result_map=result_filter.map(function(even){
    //     return even*1000;
    // }); 
    // console.log(result_map);

    const result_filter = arr.filter(number => number % 2 === 0).map(even => even * 1000);
    console.log(result_filter);
});