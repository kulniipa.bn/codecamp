$(function () {
    console.log("homework7_3");
    fetch('data/homework1-4.json').then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);

            let PersonalInfo = [];
            const result = data.filter(Personal => {
                return Personal.gender === "male" && Object.keys(Personal.friends).length >= 2
            });
            console.log(result);

            //create table
            let html_data = '<tr>';
            for (let k in result[0]) {
                console.log(k);
                html_data += '<th>' + k + '</th>';
            }
            html_data+='</tr>';

            let list;
            for (let i in result) {
                console.log(result[i]);
                html_data+='<tr>';
                for (let j in result[i]) {
                    html_data+='<td>' + result[i][j] + '</td>';
                }
                html_data+='</tr>';
            }
            $('#myTable').append(html_data);
            //create table

        })
        .catch(error => {
            console.error('Error:', error);
        });
});