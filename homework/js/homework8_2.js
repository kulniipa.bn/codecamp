'use strict'

let fs = require('fs');

let StrRobotAll = '';
let StrRobot = '';
fs.readFile('head.txt', 'utf8', ReadFileHead);

function ReadFileHead(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    StrRobot += `${data}\n`;
    fs.readFile('body.txt', 'utf8', ReadFileBody);
}

function ReadFileBody(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    StrRobot += `${data}\n`;
    fs.readFile('leg.txt', 'utf8', ReadFileFeet);
}

function ReadFileFeet(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    StrRobot += `${data}\n`;
    fs.readFile('feet.txt', 'utf8', WriteFileRobot);
}

function WriteFileRobot(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    StrRobot += `${data}\n`;
    fs.writeFile('robot.txt', StrRobot, 'utf8', WriteFileCompleted);
}

function WriteFileCompleted(err) {
    console.log("Write robot.txt");
}


//*Recursive function
// //function ReadFileText() {
//     let arrfileName=['head.txt','body.txt','leg.txt','feet.txt'];
//     for(let i in arrfileName){
//         fs.readFile('head1.txt', 'utf8', ReadFileHead);
//         function ReadFileHead(err, data) {
//             if (err) {
//                 console.error(err);
//                 return;
//             }
//             StrRobot += `${data}\n`;
//             //fs.readFile('body.txt', 'utf8', ReadFileBody);
//         }
//         // fs.readFile(arrfileName[i], 'utf8', ReadFileText);
//         // if (err) {
//         //     console.error(err);
//     fs.writeFile('robot.txt', StrRobotAll, 'utf8', function WriteFileCompleted(err) {
//         console.log("Write robot.txt");
//     });
// //}