const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : 'nI<5!!My$q1',
    database        : 'codecamp'
})

router.get('/instructor/find_all', async ctx => {
    const [rows] = await pool.query('SELECT * FROM instructors')
    ctx.body = rows
})

router.get('/instructor/find_by_id/:id', async ctx => {
    const [rows] = await pool.query('SELECT * FROM instructors WHERE id =?', [ctx.params.id])
    ctx.body = rows
})

router.get('/course/find_by_id/:id', async ctx => {
    const [rows] = await pool.query(`SELECT * FROM courses WHERE id = ?`, [ctx.params.id])
    ctx.body = rows
})

router.get('/course/find_by_price/:price', async ctx => {
    const [rows] = await pool.query(`SELECT * FROM courses WHERE price = ?`, [ctx.params.price])
    ctx.body = rows
})

app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3000)