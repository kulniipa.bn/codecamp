const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const pool = require('./lib/db')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/instructor/find_all', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * from instructors');
    ctx.body = rows;
    await next();
});
router.get('/instructor/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * from instructors WHERE id=?',[ctx.params.id]);
    ctx.body = rows;
    await next();
});
router.get('/course/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE id=?',[ctx.params.id]);
    ctx.body = rows;
    await next();
});
router.get('/course/find_by_price/:price', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE price=?',[ctx.params.price]);
    ctx.body = rows;
    await next();
});

 
router.get('/',async (ctx,next)=>{
    let all_price = await getAllPrice()
    let price = await getPrice()

    let obj1 = {}
    obj1.price1 = all_price
    let obj2 = {}
    obj2.price2 = price
    let data = {price1:obj1.price1,price2:obj2.price2}
    console.log(data)
    await ctx.render('quiz01',data)
})

async function getAllPrice(){
    [rows] = await pool.query(`select sum(c.price) as price from courses as c
                            inner join enrolls as e on e.course_id=c.id`)
    return rows
}

async function getPrice(){
    [rows] = await pool.query(`select s.name,sum(c.price) as price from courses as c
                                inner join enrolls as e on e.course_id=c.id
                                inner join students as s on e.student_id=s.id
                                group by s.name`)
    return rows
}

app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(5000)