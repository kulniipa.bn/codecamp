import React,{Component} from 'react'

class PictureCard  extends Component{
    render(){
        return(
            <div className="card" key={this.props.id}>
                    <p className="profile-name">{this.props.createdBy}</p>
                    <p className="posted-date">{this.props.createdAt}</p>
                    <a href="http://www.google.com">
                        <img src={this.props.picture} alt='photos'/>
                    </a>
                    <div>
                        <span className='likes'>{this.props.likeCount} likes</span>
                        <span className='comments'>{this.props.commentCount} comments</span>
                    </div>            
            </div>
        )
    }
}

export {PictureCard}