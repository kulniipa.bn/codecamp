import React,{Component} from 'react'
import CommentList from './CommentList'

class PictureCardById  extends Component{
    state = {
        values: []
    }
    
    handleClick = () => {
        this.setState({
          values: [...this.state.values, Math.random()]
        })
    }

    handleChange = (e) =>{
        this.setState({value: e.target.value});
    }

    render(){
        console.log(this.props.comments)
        const { values } = this.state
        return(
            <div className="card-large" key={this.props.id}>
                    <p className="profile-name">{this.props.createdBy}</p>
                    <p className="posted-date">{this.props.createdAt}</p>
                    <a href="http://www.google.com">
                        <img src={this.props.picture} alt='photos'/>
                    </a>
                    <div>
                        <span className='likes'>{this.props.likeCount} likes</span>
                        <span className='comments'>{this.props.commentCount} comments</span>
                    </div> 
                    <form className="comment">
                        <CommentList values={values} />
                        <textarea rows="2" cols="50" value={this.state.value} onChange={this.handleChange}></textarea>
                        <button onClick={this.handleClick}>comment</button>  
                    </form>         
            </div>
        )
    }
}

export {PictureCardById}