import React,{Component} from 'react'

class UserGenerateApp extends Component{
    state = { picture: '',email:'',gender:'',name:'' }
    getRandomUser = async () =>{
        const response = await fetch('https://randomuser.me/api/') 
        const strUser = await response.json()

        this.setState(
            { 
                picture: strUser.results[0].picture.large,
                email: strUser.results[0].email,
                gender: strUser.results[0].gender,
                name: strUser.results[0].name.title + " " +  strUser.results[0].name.first + " " + strUser.results[0].name.last
            }
        );
    }
    
    render(){
        return(
            <div>
                <img src={this.state.picture}/>
                <p>Email : {this.state.email}</p>
                <p>Gender : {this.state.gender}</p>
                <p>Name : {this.state.name}</p>
                <button onClick={this.getRandomUser}>get</button>
            </div>
        )
    }
}

export {UserGenerateApp}