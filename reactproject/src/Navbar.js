import React,{Component} from 'react'
import {Logo} from './Logo.js'
import './navbar.css'
import {NavLink} from 'react-router-dom'

class Navbar extends Component{
    render(){
        return(
            <div className="topnav">
                <NavLink to='/' exact className="brand-logo"><Logo/></NavLink>
                <NavLink to='/create' exact>Create</NavLink>
                <NavLink to='/pikka' exact>Pikka</NavLink>
                <NavLink to='/signup' exact>Signup</NavLink>
                <NavLink to='/signin' exact>Signin</NavLink>
                <NavLink to='/signout' exact>Signout</NavLink>
            </div>            
        ) 
    }
}

export {Navbar}
