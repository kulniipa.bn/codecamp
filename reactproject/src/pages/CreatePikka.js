import React,{ Component } from 'react'

class CreatePikka extends Component{
    state = {
        picture: '',
        caption: ''
    }
    handleSubmit = e => {
        e.preventDefault()
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    onSubmit = () => {
        this.setState({
            email: this.state.email, password: this.state.password, confrim: this.state.confrim 
        })
    }  
    postForm = async () =>{
        const formData = new FormData()
        const pictureFile = document.getElementById('picture').files[0]
        formData.append('caption', 'Yo!')
        formData.append('picture', pictureFile)
        try {
          const response = await fetch('http://35.240.130.197/api/v1/pikka', {
            method: 'post',
            body: formData,
            credentials: 'include'
          })
          const data = await response.json()
          this.props.history.push('/')
        }catch (err) {
            console.log(err)
        }
    }
       
    render(){
        const {
            picture,
            caption
        } = this.state
           
        return(
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="caption">Caption : </label>
                <input type='text' className='caption' name='caption' value={caption} onChange={this.handleChange}></input>
                <input id='picture' type='file' ref={(input) => this.textInput = input} />
                <button onClick={this.postForm}>Create</button>
            </form>
        )
    }
}

export {CreatePikka}