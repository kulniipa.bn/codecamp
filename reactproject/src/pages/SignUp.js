import React,{ Component } from 'react'

class SignUp extends Component{
    state = {
        email: '',
        password: '',
        confrim:''
    }
    handleSubmit = e => {
        e.preventDefault()
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    onSubmit = () => {
        this.setState({
            email: this.state.email, password: this.state.password, confrim: this.state.confrim 
        })
    }

    createUser = async() =>{
        try {
            const response = await fetch('http://35.240.130.197/api/v1/auth/signup', {
            method: 'post',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ email: this.state.email , password: this.state.password }),
            credentials: 'include'
          })
          const data = await response.json()
        }catch (err) {
            console.log(err)
        }
    }

    render(){
        const {
            email,
            password,
            confrim
        } = this.state

        return(
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="email">Email : </label>
                <input className="email" name="email" type="email" value={email} onChange={this.handleChange} required></input>
                <label htmlFor="password">Password : </label>
                <input className="password" name="password" type="password" value={password} onChange={this.handleChange} required></input>
                <label htmlFor="confirmPassword">Confirm Password : </label>
                <input className="confirmPassword" name="confrim" type="password" value={confrim} onChange={this.handleChange} required></input>
                <button onClick={this.createUser}>sigup</button>
            </form>
        )
    }
}

export {SignUp}