import React,{ Component } from 'react'
import {connect} from "react-redux"
import * as action from "../actions"

class SignIn extends Component{
    state = {
        email: '',
        password: ''
    }
    handleSubmit = e => {
        e.preventDefault()
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    onSubmit = () => {
        this.setState({
            email: this.state.email, password: this.state.password
        })
    }
    
    signIn = async () => {
            try {
                const response = await fetch('http://35.240.130.197/api/v1/auth/signin', {
                method: 'post',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({ email: this.state.email , password: this.state.password }),
                credentials: 'include'
              })
              const data = await response.json()
              this.props.history.push('/')
              //this.props.setAuth(true)

              this.props.isAuth(true)
              //(response.status==200) ? this.props.isAuth(true) : this.props.isAuth(false);
            }catch (err) {
                console.log(err)
            }
        }

    render(){
        const {
            email,
            password
        } = this.state
           
        return(
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="email">Email : </label>
                <input className="email" name="email" type="email" value={email} onChange={this.handleChange} required></input>
                <label htmlFor="password">Password : </label>
                <input className="password" name="password" type="password" value={password} onChange={this.handleChange} required></input>
                <button onClick={this.signIn}>sigin</button>
            </form>
        )
    }
}

//export {SignIn}
//export default SignIn
const mapStateToProps = state => {
  console.log(state);
  return {
    isAuth: state.isAuth
  };
};
export default connect(
  mapStateToProps,
  action
)(SignIn);