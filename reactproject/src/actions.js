    //action
    function isAuth(status){
        return {type:'AUTHEN',isAuth:status}
    }

    function getUser(email){
        return {type:'USER',picture:'picture',email:email,gender:'gender',name:'name'}
    }

    function incrementBy(number) {
        return { type: 'INCREMENT', amount: number }
    }
      
    export {
        isAuth,
        getUser,
        incrementBy
    }
