import React from "react";

class CommentList extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.values !== this.props.values;
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("ValueList updated");
  }

  render() {
    const { values } = this.props;
    return <ul>{values.map((v, i) => <li key={v + i}>value: {v}</li>)}</ul>;
  }
}

export default CommentList
