import React,{Component} from 'react'
import {CardDect} from './CardDeck.js'

class CardDeckApp extends Component{
    state = { id:'', color: '',name:'' }

    handleClick = (e) => {
        alert(e.target.name)
    }

    shuffle = ([...arr]) => {
        let m = arr.length;
        while (m) {
          const i = Math.floor(Math.random() * m--);
          [arr[m], arr[i]] = [arr[i], arr[m]];
        }
        return arr;
    };

    render(){
        return(
            <CardDect onClick={this.handleClick} suffle={this.shuffle}/>
        )
    }
}

export {CardDeckApp}