import React,{Component} from 'react'
import {PictureCard} from './PictureCard.js'
import {Footer} from './Footer.js'
import './PictureCard.css'

class Dashboard extends Component{
    state ={
        details: '',
        isLoading: true
    }

    async componentDidMount() {
        try {
            const response = await fetch('http://35.240.130.197/api/v1/pikka')
            const {list:[...pictures]} = await response.json()
            this.setState({details:pictures})
          } catch (err) {
            console.log(err.message);
          }finally {
            this.setState({ isLoading: false });
          }   
    }

    render(){  
        const {details,isLoading} = this.state
        if (isLoading) return <div>Loading...</div>;
        return(
            <div>
                <header className="App-header">
                </header>
                <div className="App-intro">   
                    <div className="box">                 
                        {this.state.details.map(detail =>
                            <PictureCard key={detail.id}
                                id={detail.id} 
                                createdAt={detail.createdAt}
                                createdBy={detail.createdBy} 
                                picture={detail.picture}
                                likeCount={detail.likeCount}
                                commentCount={detail.commentCount}
                            />
                        )}
                    </div>                 
                </div>
                <div className="App-Footer">
                    <Footer/>
                </div>
            </div>
        )
    }
}

export {Dashboard}