import React from 'react'
import { connect } from 'react-redux'
import {incrementBy} from './actions'

const ToDoApp = props => (
  <div>
    <h1>Count in State is: {props.count}</h1>
    <button onClick={() => props.incrementBy(2)}>increment</button>
  </div>
)

const mapStateToProps = state => {
  return {
    count: state.count
  }
}

export default connect(
  mapStateToProps,
  incrementBy
)(ToDoApp)
